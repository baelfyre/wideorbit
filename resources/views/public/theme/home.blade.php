@extends('public.master')
@section('body')
  @foreach($plugins as $plugin_key => $plugin_data)
    @foreach($plugin_data as $plugin_name => $plugin_content)
      @include('public.theme.plugins.'.$plugin_name.'', ['content' => $plugin_content])
    @endforeach
  @endforeach
@endsection
