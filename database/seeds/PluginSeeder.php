<?php

use Illuminate\Database\Seeder;

class PluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plugins')->insert([
          [
            'pages_id' => 1,
            'position' => 1,
            'name' => 'pages',
            'content' => '<h1>Your new website is ready!</h1><p>This is your home page. Below is the Gallery plugin displaying some images.  After that is another instance of the Pages plugin that provides text.</p>',
            'status'  => 'Active', //['Pending','Active'])
          ],

          [
            'pages_id' => 1,
            'position' => 2,
            'name' => 'gallery',
            'content' => json_encode(['img' => ['/img/sample_1.jpg','/img/sample_2.jpg','/img/sample_3.jpg']]),
            'status'  => 'Active', //['Pending','Active'])
          ],

          [
            'pages_id' => 1,
            'position' => 3,
            'name' => 'text',
            'content' => '<h2>The other Pages Plugin</h2><p>As mentioned in the first Pages plugin above, this is where can blab about whatever we like.',
            'status'  => 'Active', //['Pending','Active'])
          ],

          [
            'pages_id' => 2,
            'position' => 1,
            'name' => 'pages',
            'content' => '<h1>About</h1><p>A website isn\'t complete without the arbitrary about page</p>',
            'status'  => 'Active', //['Pending','Active'])
          ],

          [
            'pages_id' => 3,
            'position' => 1,
            'name' => 'pages',
            'content' => '<h1>About</h1><p>A website isn\'t complete without the arbitrary about page</p>',
            'status'  => 'Active', //['Pending','Active'])
          ],

          [
            'pages_id' => 4,
            'position' => 1,
            'name' => 'pages',
            'content' => '<h1>Contact Us</h1><p>Reach out to us.</p>',
            'status'  => 'Active', //['Pending','Active'])
          ],

          [
            'pages_id' => 4,
            'position' => 2,
            'name' => 'form',
            'content' => json_encode(['field'=>['text','name'],'field'=>['email','email'],'field'=>['textarea','message']]),
            'status'  => 'Active', //['Pending','Active'])
          ],

          [
            'pages_id' => 5,
            'position' => 1,
            'name' => 'blog_list',
            'content' => null,
            'status'  => 'Active', //['Pending','Active'])
          ],

          [
            'pages_id' => 6,
            'position' => 1,
            'name' => 'blog_post',
            'content' => '<h1>Sample Blog Post</h1><p>This is a sample blog post for you to check out.  You can update this one and repurprose it, or you can delete it and create a new one.  Either way, it\'s all up to you!',
            'status'  => 'Active', //['Pending','Active'])
          ],

          [
            'pages_id' => 6,
            'position' => 2,
            'name' => 'blog_comments',
            'content' => json_encode(['author'=>'example@email.com','published'=>date('m-d-Y H:i:s'),'comment'=>'This is a comment about that amazing blog post.  You have full control over moderating who can comment so you don\'t get undesirable spammy comments']),
            'status'  => 'Active', //['Pending','Active'])
          ],

          [
            'pages_id' => 7,
            'position' => 1,
            'name' => 'pages',
            'content' => 'Put your company privacy policy here for your visitors.',
            'status'  => 'Active', //['Pending','Active'])
          ],

        ]);
    }
}
