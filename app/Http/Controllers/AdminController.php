<?php

namespace Baelfyre\Http\Controllers;

use Illuminate\Http\Request;
use Baelfyre\Http\Requests;
use Baelfyre\Page;
use Baelfyre\Plugin;

class AdminController extends Controller
{
  public function getDashboard() {

    $pages = new Page;
    $active_count= $pages::where('status','Active')->count();
    $pending_count= $pages::where('status','Pending')->count();

    $plugins = new Plugin;
    $pending_comments = $plugins::where('name','blog_comments')
      ->where('status','Pending')->count();

    $content = [
      'active_page_count' => $active_count,
      'pending_page_count' => $pending_count,
      'pending_comment_count' => $pending_comments,
      'page_title' => 'Dashboard',
    ];

    return view('admin.pages.dashboard',$content);
  }

  public function getCreate($slug) {

  }

  public function getList($slug) {
    $return_list = '';
    $page_title = '';

    if($slug == 'pages') {
      $pages = new Page;
      $return_list = $pages::where('id','>',0)
      ->orderBy('name','asc')
      ->paginate(10);
      $page_title = 'Pages';
    }

    $content = [
      'return_list' => $return_list,
      'page_title' => $page_title,
    ];

    return view('admin.pages.list',$content);

  }

  public function getEditItem($slug, $id) {
    $page = '';
    $edit_type = '';
    $page_title = '';
    $page_plugins = '';
    
    if($slug == 'page') {
      $pages = new Page;
      $plugins = new Plugin;

      $page = $pages::find($id);
      $edit_type = 'page';
      $page_title = $page->name;
      $page_plugins = $plugins::where('pages_id',$page->id)->get();
    }


    $content = [
      'edit_type' => $edit_type,
      'return_page' => $page,
      'return_plugins' => $page_plugins,
      'page_title' => $page_title,
    ];

    return view('admin.pages.edit',$content);
  }

  public function getDeleteItem($slug) {

  }

  public function postCreateNew(Request $request, $slug) {

  }

  public function postCreateUpdate($slug) {

  }

}
