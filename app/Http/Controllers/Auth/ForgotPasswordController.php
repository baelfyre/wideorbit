<?php

namespace Baelfyre\Http\Controllers\Auth;

use Baelfyre\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // protected $linkRequestView = 'public.auth.passwords.email';
    //protected $resetView = 'public.auth.passwords.reset';
    public function __construct()
    {
        $this->middleware('guest');
    }
}
