<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Baelfyre Spark - {{$page_title}}</title>

<link href="/dashboard/css/bootstrap.min.css" rel="stylesheet">
<link href="/dashboard/css/styles.css" rel="stylesheet">
<!--Icons-->
<script src="/dashboard/js/lumino.glyphs.js"></script>
<!--[if lt IE 9]>
<script src="js/dashboard/html5shiv.js"></script>
<script src="js/dashboard/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img src="/dashboard/logo.png" style="margin-top: -5px" />Baelfyre Spark</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> {{\Auth::user()->name}} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>

		</div><!-- /.container-fluid -->
	</nav>

	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="/admin"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>

			<li class="parent ">
				<a href="#">
					<span data-toggle="collapse" href="#sub-item-1"><svg class="glyph stroked star"><use xlink:href="#stroked-star"/></svg> Create</span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li>
						<a class="" href="/admin/create/page">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> New Page
						</a>
					</li>
					<li>
						<a class="" href="/admin/create/post">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> New Blog Post
						</a>
					</li>
					<li>
						<a class="" href="/admin/create/gallery">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> New Gallery
						</a>
					</li>
					<li>
						<a class="" href="/admin/create/form">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> New Form
						</a>
					</li>
				</ul>
			</li>

			<li class="parent ">
				<a href="#">
					<span data-toggle="collapse" href="#sub-item-2"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"/></svg> Edit</span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li>
						<a class="" href="/admin/list/pages">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Pages
						</a>
					</li>
					<li>
						<a class="" href="/admin/list/posts">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Blog Posts
						</a>
					</li>
					<li>
						<a class="" href="/admin/list/comments">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Blog Comments
						</a>
					</li>
					<li>
						<a class="" href="/admin/list/contacts">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Contacts
						</a>
					</li>
					<li>
						<a class="" href="/admin/list/media">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Media Library
						</a>
					</li>
				</ul>
			</li>

			<li class="parent ">
				<a href="#">
					<span data-toggle="collapse" href="#sub-item-3"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"/></svg> Site Settings</span>
				</a>
				<ul class="children collapse" id="sub-item-3">
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> General
						</a>
					</li>
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Integrations
						</a>
					</li>
					<li>
						<a class="" href="#">
							<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Users
						</a>
					</li>
				</ul>
			</li>

			<li role="presentation" class="divider"></li>
			<li><a href="/logout"><svg class="glyph stroked flag"><use xlink:href="#stroked-flag"/></svg> Submit A Support Ticket</a></li>
			<li><a href="/logout"><svg class="glyph stroked arrow left"><use xlink:href="#stroked-arrow-left"/></svg> Logout</a></li>
		</ul>

	</div><!--/.sidebar-->
  <!-- start content wrapper -->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">

		<div class="row">
			<div class="col-lg-12">
				@yield('body')
			</div>
		</div><!--/.row-->

	</div>	<!--/.main-->
  <!-- end content wrapper -->
	<script src="/dashboard/js/jquery-1.11.1.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="/dashboard/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/d31c4bbe3b.js"></script>

	@stack('js')
</body>

</html>
