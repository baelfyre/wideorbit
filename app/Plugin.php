<?php

namespace Baelfyre;

use Illuminate\Database\Eloquent\Model;

class Plugin extends Model
{
    public function page() {
      return $this->belongsTo('Baelfyre\Page');
    }
}
