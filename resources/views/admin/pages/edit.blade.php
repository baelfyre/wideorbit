@extends('admin.layouts.master')

@section('body')
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editing: {{$page_title}}</div>
      <div class="panel-body">
        @if($edit_type == 'page')
          @include('admin.partials.pagesedit')
        @endif
      </div>
    </div>
  </div>
</div>
@endsection
