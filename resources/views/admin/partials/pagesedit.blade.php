<div class="row">

  <div class="col-md-12">
    <form name="page_edit" method="post" action="create/update/page">
      <button class="btn btn-info pull-right" type="submit">Update Page</button>
      <h3>Page Configuration</h3>
      <label for="page_name">Page Name</label>
      <input type="text" class="form-control" name="page_name" value="{{$return_page->name}}" placeholder="This page name is what you will see while in the admin">
      <div class="col-md-12">
        <div class="col-md-2">
          <label for="page_category">Category</label>
          <input type="text" class="form-control" name="page_category" value="{{$return_page->category}}" placeholder="">
        </div>
        <div class="col-md-2">
          <label for="page_category">Slug</label>
          <input type="text" class="form-control" name="page_slug" value="{{$return_page->slug}}" placeholder="">
        </div>
      </div>
      <label for="page_layout">Page Layout</label>
      <select class="form-control" name="page_layout">
        <option>Please Select</option>
        <option value="default">Default</option>
        <option value="column_left">Sidebar On The Left, Content On The Right</option>
        <option value="column_right">Sidebar On The Right, Content On The Left</option>
        <option value="three_column">Columns On The Left And Right, Content On The Center</option>
      </select>
      <hr />
      <h3>Page Content</h3>
      <div id="sortable">
      @foreach($return_plugins as $plugin_key => $plugin_val)
      <div class="panel ui-state-default">
        <div class="panel-heading">
          {{$plugin_val->name}}
        </div>
        <div class="panel-body">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ut ante in sapien blandit luctus sed ut lacus. Phasellus urna est, faucibus nec ultrices placerat, feugiat et ligula. Donec vestibulum magna a dui pharetra molestie. Fusce et dui urna.</p>
        </div>
      </div>
z
      @endforeach
    </div>
      <br />
      <button class="btn btn-info pull-right" type="submit">Update Page</button>
    </form>
</div>
@push('js')
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
  $( "#sortable" ).sortable();
  $( "#sortable" ).disableSelection();
} );
</script>
@endpush
