<?php

namespace Baelfyre;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
  public function plugins() {
    return $this->hasMany('Baelfyre\Plugin','pages_id', 'id');
  }
}
