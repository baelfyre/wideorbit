<?php

use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('pages')->insert([
        /* 1 - home page */
        [
          'category' => null,
          'slug' => 'home',
          'name' => 'Home Page',
          'layout' => 'home',
          'created_by' => 1,
          'updated_by' => 1,
          'status' => 'Active', //['Pending','Active','Archived']);
        ],
        /* 2 - about page */
        [
          'category' => null,
          'slug' => 'about',
          'name' => 'About Page',
          'layout' => 'column_left',
          'created_by' => 1,
          'updated_by' => 1,
          'status' => 'Active', //['Pending','Active','Archived']);
        ],
        /* 3 - services page */
        [
          'category' => 'about',
          'slug' => 'services',
          'name' => 'Services Page',
          'layout' => 'column_left',
          'created_by' => 1,
          'updated_by' => 1,
          'status' => 'Pending', //['Pending','Active','Archived']);
        ],
        /* 4 - contact page */
        [
          'category' => null,
          'slug' => 'contact',
          'name' => 'Contact Page',
          'layout' => 'default',
          'created_by' => 1,
          'updated_by' => 1,
          'status' => 'Active', //['Pending','Active','Archived']);
        ],
        /* 5 - blog placeholder page */
        [
          'category' => null,
          'slug' => 'blog',
          'name' => 'Blog Landing Page',
          'layout' => 'column_right',
          'created_by' => 1,
          'updated_by' => 1,
          'status' => 'Active', //['Pending','Active','Archived']);
        ],
        /* 6 - example blog post */
        [
          'category' => 'blog',
          'slug' => 'example-blog-post',
          'name' => 'Example Blog Post',
          'layout' => 'column_right',
          'created_by' => 1,
          'updated_by' => 1,
          'status' => 'Active', //['Pending','Active','Archived']);
        ],
        /* 7 - privacy policy */
        [
          'category' => 'about',
          'slug' => 'privacy-policy',
          'name' => 'Privacy Policy Page',
          'layout' => 'column_right',
          'created_by' => 1,
          'updated_by' => 1,
          'status' => 'Pending', //['Pending','Active','Archived']);
        ],

      ]);
    }
}
