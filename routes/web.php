<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('404','GuestController@get404');
Auth::routes();
/***ADMIN***/

Route::group(['middleware' => 'auth'], function () {

  Route::group(['prefix' => 'admin'], function () {
    // crud pages
    Route::get('/', 'AdminController@getDashboard');
    Route::get('create/{slug}', 'AdminController@getCreate');
    Route::get('list/{slug}', 'AdminController@getList');
    Route::get('list/edit/{slug}/{id}', 'AdminController@getEditItem');
    Route::get('list/delete/{slug}/{id}', 'AdminController@getDeleteItem');
    Route::post('create/new/{slug}', 'AdminController@postCreateNew');
    Route::post('create/update/{slug}', 'AdminController@postCreateUpdate');
  });

});

/***PUBLIC***/


//Route::get('login', 'GuestController@getLogin');
Route::get('/', 'GuestController@getHome');
Route::get('{category}/{slug}', 'GuestController@getCategoryPage');
Route::get('{slug}', 'GuestController@getPage');



Route::get('/home', 'HomeController@index');
