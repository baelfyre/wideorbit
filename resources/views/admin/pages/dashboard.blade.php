@extends('admin.layouts.master')

@section('body')
<div class="row">
  <div class="col-lg-12">
    <h2>Site Overview</h2>
  </div>
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="panel panel-blue panel-widget ">
      <div class="row no-padding">
        <div class="col-sm-3 col-lg-5 widget-left">
          <svg class="glyph stroked app window with content"><use xlink:href="#stroked-app-window-with-content"/></svg>
        </div>
        <div class="col-sm-9 col-lg-7 widget-right">
          <div class="large"><a href="/admin/list/pages">{{$active_page_count}}</a></div>
          <div class="text-muted"><a href="/admin/list/pages">Active Pages</a></div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="panel panel-orange panel-widget">
      <div class="row no-padding">
        <div class="col-sm-3 col-lg-5 widget-left">
          <svg class="glyph stroked app window with content"><use xlink:href="#stroked-app-window-with-content"/></svg>
        </div>
        <div class="col-sm-9 col-lg-7 widget-right">
          <div class="large">{{$pending_page_count}}</div>
          <div class="text-muted">Pending Pages</div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="panel panel-red panel-widget">
      <div class="row no-padding">
        <div class="col-sm-3 col-lg-5 widget-left">
          <svg class="glyph stroked email"><use xlink:href="#stroked-email"/></svg>
        </div>
        <div class="col-sm-9 col-lg-7 widget-right">
          <div class="large">12</div>
          <div class="text-muted">New Contacts</div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="panel panel-teal panel-widget">
      <div class="row no-padding">
        <div class="col-sm-3 col-lg-5 widget-left">
          <svg class="glyph stroked two messages"><use xlink:href="#stroked-two-messages"/></svg>
        </div>
        <div class="col-sm-9 col-lg-7 widget-right">
          <div class="large">{{$pending_comment_count}}</div>
          <div class="text-muted">Blog Comments</div>
        </div>
      </div>
    </div>
  </div>

</div><!--/.row-->

<div class="row">
  <div class="col-lg-12">
    <h2>Help &amp; Billing</h2>
  </div>

  <div class="col-md-6">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <svg class="glyph stroked flag"><use xlink:href="#stroked-flag"/></svg>Help
      </div>
      <div class="panel-body">
        <p><strong>Got questions? Don't worry, we got answers! If your question isn't covered here, start up a support ticket and we will answer ASAP.</strong></p>
        <ul style="list-style-type:square">
          <li><a href="#">How do I create pages?</a></li>
          <li><a href="#">How do I edit existing pages?</a></li>
          <li><a href="#">How do I delete pages?</a></li>
          <li><a href="#">Do the plugins have instructions on how to use them?</a></li>
          <li><a href="#">I created a page but it's not visible on my website.</a></li>
          <li><a href="#">Any tips on how to get more traffic to my site?</a></li>
          <li><a href="#">How do I add a user to my site?</a></li>
          <li><a href="#">I need to update my design. Can you do it?</a></li>
          <li><a href="#">I need a new feature added. Can you add it?</a></li>
          <li><a href="#">Are there any other services you guys offer that would help my website?</a></li>
          <li><a style="color:red" href="#">I don't see my question here. I want to open a support ticket.</a></li>
        </ul>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="panel panel-success">
      <div class="panel-heading">
        <svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"/></svg>Billing - $100 Due on the 26th
      </div>
      <div class="panel-body">
        <h4>Make A Payment</h4>
        <form>
          <div class="col-md-12">
            <label for="cc_name">Name On Card</label>
            <input type="text" class="form-control" name="cc_name">
          </div>
          <div class="col-md-12">
            <label for="cc_number">Credit Card Number</label>
            <input type="text" class="form-control" name="cc_name">
          </div>
          <div class="col-md-4">
            <label for="cc_year">CC Month</label>
            <select class="form-control">
              <option value="1">January</option>
              <option value="2">February</option>
              <option value="3">March</option>
              <option value="4">April</option>
              <option value="5">May</option>
              <option value="6">June</option>
              <option value="7">July</option>
              <option value="8">August</option>
              <option value="9">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
            <select>
          </div>
          <div class="col-md-4">
            <label for="cc_year">CC Year</label>
            <select class="form-control">
              <option value="2016">2016</option>
              <option value="2017">2017</option>
              <option value="2018">2018</option>
              <option value="2019">2019</option>
              <option value="2020">2020</option>
              <option value="2021">2021</option>
              <option value="2022">2022</option>
              <option value="2023">2023</option>
              <option value="2024">2024</option>
            <select>
          </div>
          <div class="col-md-4">
            <label for="cc_csv">3 Digit Code</label>
            <input type="text" class="form-control" maxlength="3" name="cc_name">
          </div>
          <div class="col-md-12">
            <br />
            <button class="btn btn-success" type="submit">Submit Payment</button>
          </div>
        </form>
      </div>
    </div>
  </div>

</div><!-- /.row -->
@endsection
