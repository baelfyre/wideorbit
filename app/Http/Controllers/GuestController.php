<?php

namespace Baelfyre\Http\Controllers;
use Illuminate\Http\Request;
use Baelfyre\Http\Requests;
use Baelfyre\Page;
use Baelfyre\Plugin;
use Baelfyre\Auth\LoginController;
use Baelfyre\Auth\RegisterController;
use Baelfyre\Auth\ForgotPasswordController;
use Baelfyre\Auth\ResetPasswordController;

class GuestController extends Controller
{

    public function getHome() {
      $pages = new Page;
      $plugin = new Plugin;
      $page = $pages::where('slug','home')->first();
      $plugins = $plugin::where('pages_id',$page->id)->get();
      foreach($plugins as $key => $val) {
        if($val->name == 'gallery' || $val->name == 'form') {
          $content = json_decode($val->content);
        } else {
          $content = $val->content;
        }

        $plugin_obj[] = [$val->name => $content];
      }
      //   'plugins' => ['pages'=>'Here is the first plugin', 'gallery' => 'Here is the next plugin']
      // ];

      $plugins_arr = [
        'plugins' => $plugin_obj
      ];

      return view('public.theme.home',$plugins_arr);
    }

    public function getPage($slug) {
      $pages = new Page;
      $plugin = new Plugin;
      $page = $pages::where('slug',$slug)->first();
      if(count($page) < 1) {
        return redirect()->action('GuestController@get404');
      }
      $plugins = $plugin::where('pages_id',$page->id)->get();
      foreach($plugins as $key => $val) {
        if($val->name == 'gallery' || $val->name == 'form') {
          $content = json_decode($val->content);
        } else {
          $content = $val->content;
        }

        $plugin_obj[] = [$val->name => $content];
      }

      $plugins_arr = [
        'plugins' => $plugin_obj
      ];

      return view('public.theme.'.$page->layout.'',$plugins_arr);
    }

    public function getCategoryPage($category,$slug) {
      $pages = new Page;
      $plugin = new Plugin;
      $page = $pages::where('category',$category)
      ->where('slug',$slug)
      ->first();
      if(count($page) < 1) {
        return redirect()->action('GuestController@get404');
      }
      $plugins = $plugin::where('pages_id',$page->id)->get();
      foreach($plugins as $key => $val) {
        if($val->name == 'gallery' || $val->name == 'form') {
          $content = json_decode($val->content);
        } else {
          $content = $val->content;
        }

        $plugin_obj[] = [$val->name => $content];
      }
      //   'plugins' => ['pages'=>'Here is the first plugin', 'gallery' => 'Here is the next plugin']
      // ];

      $plugins_arr = [
        'plugins' => $plugin_obj
      ];

      return view('public.theme.'.$page->layout.'',$plugins_arr);
    }

    public function get404() {
      return view('public.theme.404');
    }
}
