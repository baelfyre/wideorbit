@foreach($content as $content_key => $form)
  @foreach($form as $field_type => $field_name)
  @if($field_type == 'textarea')
  <textarea name="{{$field_name}}" class="form-control" id="textarea_{{$field_name}}"></textarea>
  @else
  <input type="{{$field_type}}" class="form-control" name="{{$field_name}}" id="{{$field_type}}_{{$field_name}}" />
  @endif

  @endforeach
@endforeach
