@extends('admin.layouts.master')

@section('body')
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">{{$page_title}} List</div>
      <div class="panel-body">
        @foreach($return_list as $list)
        @if($page_title == 'Pages')
          @include('admin.partials.pageslist')
        @endif
          <hr />
        @endforeach
        {{ $return_list->links() }}
      </div>
    </div>
  </div>
</div>
@endsection
