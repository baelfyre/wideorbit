<div class="col-md-1">
  @if($list->status == 'Active')
      <i title="Published" style="font-size: 3.5em; color: green;" class="fa fa-check-circle-o" aria-hidden="true"></i>
  @endif

  @if($list->status == 'Pending')
  <i title="Pending" style="font-size: 3.5em; color: gold;" class="fa fa-clock-o" aria-hidden="true"></i>
  @endif

  @if($list->status == 'Archived')
  <i title="Archived" style="font-size: 3.5em; color: grey;" class="fa fa-eye-slash" aria-hidden="true"></i>
  @endif
</div>
<div class="col-md-7">
  <p style="font-size: 1.3em;"><a href="/admin/list/edit/page/{{$list->id}}">{{$list->name}}</a><br />
  @if($list->category !='')
    {{url('')}}/{{$list->category}}/{{$list->slug}} <a href="{{url('')}}/{{$list->category}}/{{$list->slug}}" title="View This Page In Another Tab" target="_blank"><i style="font-size: 0.6em" class="fa fa-external-link" aria-hidden="true"></i></a>
  @else
    {{url('')}}/{{$list->slug}} <a href="{{url('')}}/{{$list->slug}}" title="View This Page In Another Tab" target="_blank"><i style="font-size: 0.6em" class="fa fa-external-link" aria-hidden="true"></i></a>
  @endif
  </p>
</div>
<div class="cold-md-2">
  Created {{$list->created_at}}
</div>
<div class="cold-md-2">
  Updated {{$list->updated_at}}
</div>
